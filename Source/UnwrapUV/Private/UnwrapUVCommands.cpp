// Copyright 2017 Digi Labs (http://digilabs.io). All Rights Reserved.

#include "UnwrapUVCommands.h"

#define LOCTEXT_NAMESPACE "FUnwrapUVModule"

void FUnwrapUVCommands::RegisterCommands()
{
	UI_COMMAND(UnwrapMeshFromStaticMeshEditor, "Unwrap New UV", "Unwrap the currently open mesh into a new UV channel", EUserInterfaceActionType::Button, FInputGesture());
}

#undef LOCTEXT_NAMESPACE
